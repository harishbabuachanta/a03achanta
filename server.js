var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine
app.use(express.static(__dirname + '/assets/'));
app.use(express.static(__dirname + 'views'));
// manage our entries
var entries = [];
app.locals.entries = entries;
// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));


// GETS
// http GET (default and /new-entry)
//
app.get("/", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/index.html'));
});

app.get("/menu", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/menu.html'));
});

app.get("/about", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/about.html'));
});

app.get("/contact", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/contact.html'));
});
app.post("/contact",function(req,res){
  var api_key = 'key-e7636862bab732bdd4acd7a895a39ba8';
var domain = 'sandboxa9f9b5c74f964fba988a28a36f52fbcd.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from:req.body.username+'<postmaster@sandboxa9f9b5c74f964fba988a28a36f52fbcd.mailgun.org>',
  to: 'harish.idkpf54@gmail.com',
  // mail:req.body.email,
  subject:req.body.username+""+" has sent you a message",
  html: "<b>You got a message from </b> "+req.body.email+"<br> Message:"+req.body.message
};
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  if(!error)
    res.send("mail sent");
  else
    res.send("mail not sent");
});

});

app.get("/new-entry", function (req, res) {
  res.render("new-entry");
});

app.get("/guestbook", function (req, res) {
  res.render("guestbook");
});


// POSTS
// http POST (INSERT)
app.post("/new-entry", function (req, res) {
  if (!req.body.title || !req.body.body) {
    res.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: req.body.title,
    body: req.body.body,
    published: new Date()
  });
  res.redirect("/guestbook");
});

// 404

app.use(function (req, res) {
  res.status(404).render("404");
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
