var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var http = require('http').Server(app);

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine
app.use(express.static(__dirname + '/assets/'));
app.use(express.static(__dirname + 'views'));
// manage our entries
var entries = [];
app.locals.entries = entries;
// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));


// GETS
// http GET (default and /new-entry)
//
app.get("/", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/index.html'));
});

app.get("/menu", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/menu.html'));
});

app.get("/contact", function (req, res) {
 res.sendFile(path.join(__dirname + '/assets/contact.html'));
});

app.get("/new-entry", function (req, res) {
  res.render("new-entry");
});

app.get("/guestbook", function (req, res) {
  res.render("guestbook");
});


// POSTS
// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }

app.post("/contact",function())

  entries.push({
    title: request.body.title,
    body: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});

// 404

app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/');
});
